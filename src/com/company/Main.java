package com.company;

import com.company.model.Book;
import com.company.model.Literature;
import com.company.model.Magazine;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// 2. Создать структуру данных для хранения информации о книге (название, автор, издательство, год издания),
        // о журнале (название, тематика, год и месяц выхода в печать), о ежегоднике (название, тематика, издательство,
        // год издания). И иметь функционал вывода в консоль литературы (всю доступную информацию) с
        // фильтрацией по определённому году (год вводит пользователь с консоли).

        Literature[] literatures = Generator.generate();


        System.out.println("From which year do you want to see the literature from our collection? (we got literature from 1978 - 2015)");
        Scanner scanner = new Scanner(System.in);
        int minYear = Integer.parseInt(scanner.nextLine());
        System.out.println("Till which year do you want to see the literature from our collection? (we got literature from 1978 - 2015)");
        int maxYear = Integer.parseInt(scanner.nextLine());

        Literature.printLiteratureFromSpecificPeriod(literatures, maxYear, minYear);

        //filtration by specific type: only books
        System.out.println();
        System.out.println("ONLY BOOKS");
        for (Literature literature : literatures) {
            if (literature instanceof Book){
                System.out.println(literature.toString());
            }
        }

        //filtration by specific type: only magazines
        System.out.println();
        System.out.println("ONLY MAGAZINES");

        for (Literature literature : literatures) {
            if (literature instanceof Magazine){
                System.out.println(literature.toString());
            }
        }
    }
}
