package com.company;

import com.company.model.Book;
import com.company.model.Literature;
import com.company.model.Magazine;
import com.company.model.YearBook;

public class Generator {
    public static Literature[] generate(){
        Literature [] literatures = new Literature[6];

        literatures[0] = new Book("War and Peace", 1978, "Leo Tolstoy", "publishjer1");
        literatures[1] = new Book("Idiot", 1979, "Dostoevskiy", "unnamed");
        literatures[2] = new Book("Robinzon Cruze", 1982, "daniel defou", "unnamed");
        literatures[3] = new Magazine("GQ", 1999, 12, "luxury people");
        literatures[4] = new Magazine("Za Rulem", 1998, 12, "testing cars");
        literatures[5] = new YearBook("almanah", 2015, "1xbet", "win all the money");

        return literatures;
    }
}
