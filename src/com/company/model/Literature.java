package com.company.model;

import java.util.Objects;

public class Literature {
    protected String name;
    protected int createdYear;

    public Literature(String name, int createdYear) {
        this.name = name;
        this.createdYear = createdYear;
    }

    public int getCreatedYear() {
        return createdYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Literature that = (Literature) o;
        return createdYear == that.createdYear &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, createdYear);
    }

    @Override
    public String toString() {
        return "Literature{" +
                "name='" + name + '\'' +
                ", createdYear=" + createdYear +
                '}';
    }

    public static void printLiteratureFromSpecificPeriod(Literature [] literatures, int maxYear, int minYear){
        for (Literature literature : literatures) {
            if (literature.getCreatedYear() <= maxYear && literature.getCreatedYear() >= minYear){
                System.out.println(literature.toString());
            }
        }
    }
}
