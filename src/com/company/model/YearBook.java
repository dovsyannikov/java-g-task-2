package com.company.model;

import com.company.model.Literature;

public class YearBook extends Literature {

    private String publisher;
    private String idea;

    public YearBook(String name, int createdYear, String publisher, String idea) {
        super(name, createdYear);
        this.publisher = publisher;
        this.idea = idea;
    }

    @Override
    public String toString() {
        return "YearBook{" +
                "publisher='" + publisher + '\'' +
                ", idea='" + idea + '\'' +
                ", name='" + name + '\'' +
                ", createdYear=" + createdYear +
                '}';
    }
}
