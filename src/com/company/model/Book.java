package com.company.model;

import com.company.model.Literature;

public class Book extends Literature {
    private String author;
    private String publisher;

    public Book(String name, int createdYear, String author, String publisher) {
        super(name, createdYear);
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", name='" + name + '\'' +
                ", createdYear=" + createdYear +
                '}';
    }
}
