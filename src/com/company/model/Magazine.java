package com.company.model;

import com.company.model.Literature;

public class Magazine extends Literature {
    private int createdMonth;
    private String idea;

    public Magazine(String name, int createdYear, int createdMonth, String idea) {
        super(name, createdYear);
        this.createdMonth = createdMonth;
        this.idea = idea;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "createdMonth=" + createdMonth +
                ", idea='" + idea + '\'' +
                ", name='" + name + '\'' +
                ", createdYear=" + createdYear +
                '}';
    }
}
